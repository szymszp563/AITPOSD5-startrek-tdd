package utils;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class CityBlockDistanceTest {

    @Test
    void getCityBlockDistance() {
        //given
        int[] testValues1 = {0, 0, 1, 1};
        int expectedValue1 = 2;
        int[] testValues2 = {0, 0, -1, -1};
        int expectedValue2 = 2;
        int[] testValues3 = {0, 0, 0, 0};
        int expectedValue3 = 0;
        CityBlockDistance cityBlockDistance = new CityBlockDistance();

        //when & then
        assertEquals(cityBlockDistance.getCityBlockDistance(testValues1[0], testValues1[1], testValues1[2], testValues1[3]),
                expectedValue1);
        assertEquals(cityBlockDistance.getCityBlockDistance(testValues2[0], testValues2[1], testValues2[2], testValues2[3]),
                expectedValue2);
        assertEquals(cityBlockDistance.getCityBlockDistance(testValues3[0], testValues3[1], testValues3[2], testValues3[3]),
                expectedValue3);
    }
}